package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static void main(String[] args) throws IOException {
		//Entering path
		XSSFWorkbook Wbook = new XSSFWorkbook("C:\\Users\\sudarsab\\Downloads\\ReadExcel.xlsx");
		//Entering Sheet
		XSSFSheet sheet = Wbook.getSheet("Sheet1");
		//Getting Row Count
		int rowCount = sheet.getLastRowNum();
		System.out.println("RowCount:"+rowCount);
		//getting cell count from header count
		short colCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column Count:"+colCount);
		for (int i = 1; i <= rowCount; i++) 
		{
			XSSFRow row = sheet.getRow(i); //entering row
			for (int j = 0; j < colCount; j++)  // enter cell
			{
				XSSFCell cell = row.getCell(j); // getting value from cell
				String text = cell.getStringCellValue(); // read cell values string
				System.out.println(text);
			}
			
		}
	}

}
