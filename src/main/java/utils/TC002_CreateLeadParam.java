package utils;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethod;


public class TC002_CreateLeadParam extends ProjectMethod{
	//@Test(invocationCount=2, invocationTimeOut=30000)
	//@Test(groups="smoke")
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLeadParam";
		testDec = "Create a new Lead in leaftaps";
		author = "Gayatri";
		category = "Smoke";
		dataSheetName="ReadExcel";
	} 
	
	@Test(dataProvider="fetchData")
	public void CreateLead1(String cname, String fname, String lname) throws InterruptedException
	{
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("LinkText", "Create Lead"));
		clearAndType((locateElement("id", "createLeadForm_companyName")), cname);
		clearAndType(locateElement("id", "createLeadForm_firstName"), fname);
		clearAndType(locateElement("id", "createLeadForm_lastName"), lname);
		clearAndType(locateElement("id", "createLeadForm_generalProfTitle"), "selenium");
		selectDropDownUsingText(locateElement("xpath", "//select[@id='createLeadForm_dataSourceId']"), "Partner");
		click(locateElement("xpath", "//input[@class='smallSubmit']"));
		Thread.sleep(2000);
		/*String text = locateElement("xpath", "//span[text()='Dummyyy']").getText();
		if(text.contains("Dummyy"))
		{
			System.out.println("The names are "+text+" is matching, verified");
		}
		else
		{
			System.out.println("Not Verified");
		}*/
	}
	/*@DataProvider(name="getData")
	public String[][] fetchData()
	{
		String[][] data = new String[2][3];
		data[0][0]="Google";
		data[0][1]="Pikachu";
		data[0][2]="Hero";
		data[1][0]="Google";
		data[1][1]="Pikachu";
		data[1][2]="Hero";
		return data;
	} 
	@DataProvider(name="getData1")
	public String[][] fetchData1()
	{
		String[][] data = new String[2][3];
		data[0][0]="Pokemon";
		data[0][1]="Pika Pika";
		data[0][2]="Pikachu";
		data[1][0]="Anime";
		data[1][1]="Goku";
		data[1][2]="Naruto";
		return data;
	} */
	
}
