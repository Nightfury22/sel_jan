package utils;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {
	
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;	
	
	
	@Test
	public void runReport() 
	{
		//created a extentreport in the mentioned report folder
		html = new ExtentHtmlReporter("./report/extentreport.html");
		//creating a report to add in the folder
		extent = new ExtentReports();
		//adding the previous test cases/runs in the current report
		html.setAppendExisting(true);
		//attaching the data to the report we created
		extent.attachReporter(html);
		//Specifying the string name and description
		test=extent.createTest("TC001_Login", "Blah blah blah");
		//creating author name
		test.assignAuthor("Sudarsan");
		//creating category name
		test.assignCategory("Pikachu");
		//mentioning pass test cases
		test.pass("Blah blah blah wow its passed");
		
		//mentioning failed test cases inside the try catch block since its throwing exception if mentioned snap is not in the folder
		try {
			test.fail("No its failed",
					MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		} catch (IOException e) {
			e.printStackTrace();
		}
		extent.flush();
	}
	
}
