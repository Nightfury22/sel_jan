package utils;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class AdvanceReport {


	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;	
	public void startReport() 
	{
		//created a extentreport in the mentioned report folder
		html = new ExtentHtmlReporter("./report/extentreport.html");
		//creating a report to add in the folder
		extent = new ExtentReports();
		//adding the previous test cases/runs in the current report
		html.setAppendExisting(true);
		//attaching the data to the report we created
		extent.attachReporter(html);
	}
	@Test
	public void runReport(String testcaseName, String testdes, String author, String category) 
	{

		test=extent.createTest(testcaseName, testdes);
		//creating author name
		test.assignAuthor(author);
		//creating category name
		test.assignCategory(category);
	}

	public void logStep(String des, String status) {
		test.pass("Blah blah blah wow its passed");
	}


	public void endReport() 
	{
		extent.flush();
	}
}



