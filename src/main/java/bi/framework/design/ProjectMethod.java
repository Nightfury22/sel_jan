package bi.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;
import utils.ReadExcel;
import utils.ReadExcel_Param;

public class ProjectMethod extends SeleniumBase{
	//@BeforeMethod(groups="common")
	public String testcaseName, testDec, author, category, dataSheetName;
	@Parameters({"url", "username", "password"})
	@BeforeMethod
	public void login(String url, String uname, String pwd) 
	{
		startApp("Chrome", url);
		clearAndType(locateElement("id", "username"), uname);
		clearAndType(locateElement("id", "password"), pwd);
		WebElement eleClick = locateElement("class", "decorativeSubmit");
		eleClick.click();
		click(locateElement("LinkText", "CRM/SFA"));
	}
	//@AfterMethod(groups="common")
	@AfterMethod
	public void closeApp()
	{
		close();
	}
	@DataProvider(name="fetchData")
	public Object[][] getData(){
		return ReadExcel_Param.readData(dataSheetName);
		}
}
