package bl.framework.testcases;

import org.testng.annotations.Test;

import bi.framework.design.ProjectMethod;

public class TC005_MergeLead extends ProjectMethod{
	@Test(groups="reg")
	public void MergeLead() throws InterruptedException
	{
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("xpath", "//a[text()='Merge Leads']"));
		click(locateElement("xpath", "(//img[@alt='Lookup'])[1]"));
		switchToWindow(1);
		clearAndType(locateElement("name", "firstName"), "Dummyy");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(3000);
		String text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		switchToWindow(0);
		click(locateElement("xpath", "(//img[@alt='Lookup'])[2]"));
		switchToWindow(1);
		clearAndType(locateElement("name", "firstName"), "Vaishnavi");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(3000);
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		switchToWindow(0);
		click(locateElement("xpath", "//a[text()='Merge']"));
		switchToAlert();
		acceptAlert();
		click(locateElement("xpath", "//a[text()='Find Leads']"));
		clearAndType(locateElement("name", "id"), text);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(2000);
		String text2 = locateElement("class", "x-paging-info").getText();
		if(text2.contains("No records to display"))
		{
			System.out.println("Text Matched");
		}
		else
		{
			System.out.println("Text not matched");
		}
	}

}
