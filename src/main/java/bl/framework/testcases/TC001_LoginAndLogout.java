package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC001_LoginAndLogout extends SeleniumBase{

	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		/*WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);*/
		
		/*WebElement eleCRM = locateElement("LinkText", "CRM/SFA");
		click(eleCRM);*/
		//we can click locate element without creating object for webelement
		click(locateElement("LinkText", "CRM/SFA"));
		WebElement eleCreateLead = locateElement("LinkText", "Create Lead");
		click(eleCreateLead);
		//clearAndType(ele, data);
		//sendKeys((locateElement("id", "createLeadForm_companyName")));
		WebElement eleCmpName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCmpName, "Amazon");
		WebElement elefName = locateElement("id", "createLeadForm_firstName");
		clearAndType(elefName, "Superman");
		selectDropDownUsingText(locateElement("id", "createLeadForm_dataSourceId"), "dropdownValue");
		
	}
}








