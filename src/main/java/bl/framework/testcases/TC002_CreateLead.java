package bl.framework.testcases;

import org.testng.annotations.Test;

import bi.framework.design.ProjectMethod;


public class TC002_CreateLead extends ProjectMethod{
	@Test(invocationCount=2, invocationTimeOut=30000)
	//@Test(groups="smoke")
	public void CreateLead() throws InterruptedException
	{
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("LinkText", "Create Lead"));
		clearAndType((locateElement("id", "createLeadForm_companyName")), "Amazon");
		clearAndType(locateElement("id", "createLeadForm_firstName"), "Dummyyy");
		clearAndType(locateElement("id", "createLeadForm_lastName"), "Pika");
		clearAndType(locateElement("id", "createLeadForm_generalProfTitle"), "Poke");
		//click(locateElement("id", "//select[@id='createLeadForm_dataSourceId']"));
		selectDropDownUsingText(locateElement("xpath", "//select[@id='createLeadForm_dataSourceId']"), "Partner");
		click(locateElement("xpath", "//input[@class='smallSubmit']"));
		Thread.sleep(2000);
		String text = locateElement("xpath", "//span[text()='Dummyyy']").getText();
		if(text.contains("Dummyy"))
		{
			System.out.println("The names are "+text+" is matching, verified");
		}
		else
		{
			System.out.println("Not Verified");
		}
	}
}
