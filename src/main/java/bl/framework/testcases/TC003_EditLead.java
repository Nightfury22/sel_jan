package bl.framework.testcases;

import org.testng.annotations.Test;

import bi.framework.design.ProjectMethod;

public class TC003_EditLead extends ProjectMethod{
@Test(dependsOnMethods="bl.framework.testcases.TC002_CreateLead.CreateLead")
	//@Test(groups="sanity")
	public void EditLead() throws InterruptedException 
	{
		
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("xpath", "//a[text()='Find Leads']"));
		clearAndType(locateElement("xpath", "(//input[@name='firstName'])[3]"), "Dummyy");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(2000);
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		//get title of the page
		System.out.println(locateElement("xpath", "//div[contains(text(),'View Lead')]").getText());
		click(locateElement("xpath", "//a[text()='Edit']"));
		Thread.sleep(2000);
		clearAndType(locateElement("xpath", "(//input[@class='inputBox'])[1]"), "Test Leaff");
		click(locateElement("xpath", "(//input[@name='submitButton'])[1]"));
		boolean displayed = locateElement("xpath", "//span[contains(text(),'Test Leaff')]").isDisplayed();
		if(displayed)
		{
		System.out.println("Edit is Confirmed");	
		}
		//driver.close();
	}
	
}
